#solve different operations
class RPNCalculator
  def initialize
    @stack = []
    @value = nil
  end

  def push(value) #adds value to the stack
    @stack << value
  end

  def plus   #adds to values or more
    empty?    # check for empty array or contain only one I need to have a least to number to make the operation
    @value = @stack.pop + @stack.pop
    @stack << @value
  end

  def minus  # subtracts two value or more
    empty?
    @value = - @stack.pop + @stack.pop
    @stack << @value
  end

  def divide # divide two values
    empty?
    @value = 1.0 / @stack.pop * @stack.pop
    @stack << @value
  end

  def times
    empty?
    @value = @stack.pop * @stack.pop
    @stack << @value
  end
  def empty?
    raise "calculator is empty" if @stack.length < 2
  end

  def value
    @value
  end

  def tokens(string)
    operators = %w{+ - / *}
    string.split.collect { |token| operators.include?(token) ? token.to_sym : token.to_i }
  end

  def evaluate(string)
    array = tokens(string)
    symbols = [:+,:-,:*,:/]
    new_array = []
    array.each do |x|
      if symbols.include?(x)
        b, a = new_array.pop, new_array.pop
        new_array << symbol_math(x, a, b)
      else
        new_array << x
      end
    end
    new_array[0]
  end

  def symbol_math(symbol, a, b)
    return a+b if symbol == :+
    return a-b if symbol == :-
    return a*b if symbol == :*
    return a.to_f/ b if symbol == :/
  end

end
